import { createContext, useContext, useState, useEffect } from 'react';
import { colorNames } from "../../constants/colors";

const ColorContext = createContext(null);

const ColorProvider = ({children}) => {
    const [ textColor, setTextColor ] = useState(colorNames.BLACK);
    const [ currentColor, setCurrentColor ] = useState(localStorage.getItem('color') || colorNames.BLACK);

    useEffect(() => {
        setTextColor(currentColor);
    }, [ currentColor ]);

    const changeColor = (color) => {
        localStorage.setItem('color', color);
        setCurrentColor(color);
    }

    return (
        <ColorContext.Provider value={{color: textColor, changeColor}}>
            {children}
        </ColorContext.Provider>
    )
}

export const useColorContext = () => useContext(ColorContext);

export default ColorProvider;