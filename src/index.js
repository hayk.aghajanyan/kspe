import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import { BrowserRouter } from "react-router-dom";
import App from './App';
import {Provider} from "react-redux";
import store from "./redux/store";
import ColorProvider from "./context/textColorContext";

ReactDOM.render(
    <React.StrictMode>
        <ColorProvider>
            <BrowserRouter>
                <Provider store={store}>
                    <App/>
                </Provider>
            </BrowserRouter>
        </ColorProvider>
    </React.StrictMode>,
    document.getElementById('root')
);