import { NavLink } from "react-router-dom";

const Header = () => {
    return (
        <header className="app-header">
            <ul className="nav">
                <li>
                    <NavLink to="/" className="nav-link" exact activeClassName="nav-link-active">
                        Messages
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/checkbox" className="nav-link" activeClassName="nav-link-active">
                        CheckBox
                    </NavLink>
                </li>
            </ul>
        </header>
    )
}

export default Header;