import { useState } from 'react';
import { useSelector } from "react-redux";
import { useColorContext } from '../../context/textColorContext'

const FirstPage = () => {
    const [filter, setFilter] = useState(null);
    const { messages } = useSelector(({globalInfo}) => globalInfo);
    const { color } = useColorContext();

    const changeFilter = e => {
        setFilter(e.target.value);
    }

    return (
        <div className="container">
            <input onChange={changeFilter} className="filter-input" type="text" placeholder="find your text"/>
            <main>
                {
                    messages.map(message => {
                        const lowerCaseBody = message.body.toLowerCase();
                        if((filter && lowerCaseBody.includes(filter)) || !filter) {
                            return (
                                <div className="message" key={message.id}>
                                    <h4>Sender: {message.senderName}</h4>
                                    <div style={{color}}>{message.body}</div>
                                    <div><span className="date">Date: {message.date}</span></div>
                                </div>
                            )
                        }
                        return;
                    })
                }
            </main>
        </div>
    )
}

export default FirstPage;