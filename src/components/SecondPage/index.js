import { useState, useRef, useEffect } from 'react';
import { useColorContext } from "../../context/textColorContext";
import { colorNames } from "../../constants/colors";
import classnames from 'classnames';

const SecondPage = () => {
    const { color, changeColor } = useColorContext();
    const [isOpen, setIsOpen] = useState(false);
    const boxRef = useRef(null);

    useEffect(() => {
        document.addEventListener('click', boxToggle);
        return () => {
            document.removeEventListener('click', boxToggle);
        }
    }, [])

    const boxToggle = e => {
        if(!e.composedPath().includes(boxRef.current)) {
            setIsOpen(false);
        }
    }

    const showColors = e => {
        e.stopPropagation();
        setIsOpen(prev => !prev);
    }

    return (
        <div className="container">
            <h2>Text Color</h2>

            <div className="select-box">
                <div onClick={showColors} className="selected">Tap to select color</div>
                <div ref={boxRef} className={classnames("options-container", {
                    active: isOpen,
                })}>
                    {
                        Object.keys(colorNames).map(colorName => (
                            <div key={colorName} onClick={() => changeColor(colorName)} className={classnames('option', {
                                active: colorName === color
                            })}>
                                <input type="radio" className="radio" name="textColor"/>
                                <label htmlFor="radio">{colorName}</label>
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}

export default SecondPage;