import {createAction, createReducer} from "../../helpers/redux";

// ACTION TYPES
const SET_MESSAGES = 'SET_MESSAGES';

// ACTIONS
export const setMessages = createAction(SET_MESSAGES);
export const initMessages = () => (dispatch) => {
    fetch('/data.json')
        .then(res => res.json())
        .then(data => {
            dispatch(setMessages(data.messages))
        })
};

// REDUCER
const initialState = {
    messages: [],
};

export const globalInfo = createReducer(initialState, (state, { value }) => ({
    [SET_MESSAGES]: () => ({...state, messages: [...state.messages, ...value]}),
}));