export const colorNames = {
    BLACK: '#000000',
    PINK: '#c4577c',
    RED: '#ff0000',
    REBECCAPURPLE: '#663399',
    GREEN: '#ccf381',
    ORANGE: '#f0a07c',
    YELLOW: '#fad744',
    DARK_GREEN: '#295f2D',
    BROWN: '#4a171e',
}