import React, { useEffect } from 'react';
import { Route, Switch } from "react-router-dom";
import { useDispatch } from "react-redux";
import FirstPage from "./components/FirstPage";
import SecondPage from "./components/SecondPage";
import Header from "./components/Header";
import { initMessages } from "./redux/ducks/globalDuck";

const App = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(initMessages());
    }, []);

    return (
        <div className="app">
            <Header />
            <Switch >
                <Route path="/" exact component={FirstPage} />
                <Route path="/checkbox" component={SecondPage} />
            </Switch>
        </div>
    );
}

export default App;
